/*
* This file is a part of BipBip
*
* Copyright (C) 2023 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

#include "src/common/Config.h"
#include "src/common/ObjectDetector.h"
#include "src/common/Stopwatch.h"
#include "src/common/WebInterface.h"
#include "src/common/common.h"
#include <M5StickCPlus.h>
#include <VL53L0X.h>
#include <WiFi.h>
#include <WiFiAP.h>
#include <WiFiClient.h>
#include <Wire.h>

Config gConfig;
ObjectDetector detector;

// buffers for tcp data
const size_t rx_buf_sz = 16;
uint8_t rx_buf[rx_buf_sz];

WiFiServer server;
WiFiClient client; // hold client spawned by server

Stopwatch gStopwatch;
WebInterface web_interface(80);

// moment when a new request start (for timeout)
unsigned long request_start_time;

void setup() {
  // init some stuff
  M5.begin();

  // load config
  gConfig.load();

  // display setup
  M5.lcd.setRotation(3);
  M5.lcd.setTextSize(2);
  M5.lcd.println("BipBip Master");
  // detector init
  if (!detector.init()) {
    M5.lcd.println("Error, sensor not found");
    while (true)
      ;
  }

  // setup the pin that control the led
  pinMode(10, OUTPUT);

  // WiFi setup as Access Point
  WiFi.softAPConfig(MASTER_IP, MASTER_IP, SUBNET);
  WiFi.softAP(SSID, PASSWORD, WIFI_CHANNEL);
  server.begin(MASTER_PORT); // start the server

  web_interface.begin();
}

void loop() {
  gConfig.store();

  // read button state, update speaker .. and probably more
  M5.update();

  web_interface.cleanupClients();
  gStopwatch.update();

  // light up the led when something is present;
  digitalWrite(10, !detector.isPresentFiltered());

  // buffer for string manipualtion
  const auto print_buf_sz = 128;
  char print_buf[print_buf_sz];

  auto font_height = M5.lcd.fontHeight();
  auto bat_voltage = M5.Axp.GetBatVoltage();
  auto bat_current = M5.Axp.GetBatCurrent();
  auto bat_capacity = estimateBatCapacity(bat_voltage);

  // y coordinate we currently writing at
  auto cur_y = font_height;

  IPAddress my_ip = WiFi.softAPIP();

  detector.update();

  if (M5.BtnA.wasPressed()) {
    gStopwatch.reset();
  }

  // if no client, try to get a new one
  if (!client) {
    client = server.available();
    request_start_time = millis();
  }

  // if client have data, read it
  if (client.connected()) {
    while (client.available()) {
      uint8_t rd = client.read();
      // if something detected by remote ToF
      if (rd == 1) {
        Serial.println("remote presence event");
        if (gStopwatch.getState() == Stopwatch::State::RUN) {
          gStopwatch.stop();
          M5.Beep.tone(4000, 500);
          Serial.printf("Stoping gStopwatch after %lu ms\n",
                        gStopwatch.getDuration());
        }
      }
    }
  }

  // connection timeout
  if ((millis() - request_start_time) > 100) {
    client.stop();
    Serial.println("\nTimeout, closing connection");
  }

  // if something detected by local ToF
  if (detector.presenceEvent()) {
    Serial.println("local presence event");
    if (gStopwatch.getState() == Stopwatch::State::INIT) {
      gStopwatch.restart();
      M5.Beep.tone(4000, 100);
      Serial.printf("Starting gStopwatch %lu ms\n", millis());
    }
  }

  // update web interface if Stopwatch state have changed
  if (gStopwatch.takeStateChangedFlag()) {
    web_interface.updateMeasureState();
    web_interface.updateMeasureSpeed();
  }

  M5.lcd.setTextPadding(240); // to erase old text whe writing new

  const char *charging_msg = "";
  if (bat_current > 0.1) {
    charging_msg = "charging";
  }

  snprintf(print_buf, print_buf_sz, "Bat: %3u %% %s", bat_capacity,
           charging_msg);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "SSID: %s", SSID);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "Password: %s", PASSWORD);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "IP: %u.%u.%u.%u", my_ip[0], my_ip[1],
  //          my_ip[2], my_ip[3]);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "ToF: %4umm ", detector.distance());
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  auto time = gStopwatch.getTime();
  snprintf(print_buf, print_buf_sz, "time: %02hhu:%02hhu.%02hhu", time.min,
           time.sec, time.centi);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  snprintf(print_buf, print_buf_sz, "speed m/s: %.2f",
           gStopwatch.getSpeedMPS());
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  snprintf(print_buf, print_buf_sz, "speed km/h: %.2f",
           gStopwatch.getSpeedKmPH());
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;
}
