/*
* This file is a part of BipBip
*
* Copyright (C) 2023 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

#ifndef BIPBIP_COMMON_COMMON_H
#define BIPBIP_COMMON_COMMON_H

#include <IPAddress.h>
#include <Wire.h>

// Notes: all global variable declared here should be initialized in the main
// file.

// WiFi and network setup
const char *const SSID = "M5Stack_Ap";
const char *const PASSWORD = "66666666";
const int WIFI_CHANNEL = 1;
const IPAddress MASTER_IP = IPAddress(192, 168, 4, 1);
const IPAddress SUBNET = IPAddress(255, 255, 255, 0);
const uint16_t MASTER_PORT = 4321;

// give estimation of the battery capacity in percent from it's voltage
// return capacity by 5% step
// -1 mean under-voltage/over-discharge
// 101 mean over-voltage/over-charge
int8_t estimateBatCapacity(float voltage);

// object detection, wrap VL53L0X HAL to do some filtering

#endif // BIPBIP_COMMON_COMMON_H
