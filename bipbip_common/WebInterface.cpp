#include "WebInterface.h"
#include "common.h"
#include "html_to_c/config.html.h"
#include "html_to_c/measure.html.h"
#include "html_to_c/style.css.h"
#include <ESPAsyncWebSrv.h>

// transmit measure state through the given websocket
void transmitMeasureState(AsyncWebSocket *ws, Stopwatch::State state) {
  switch (state) {
  case Stopwatch::State::INIT:
    ws->textAll(String(R"raw({"state":"Ready for new measure"})raw"));
    break;
  case Stopwatch::State::RUN:
    ws->textAll(String(R"raw({"state":"Measure in progress"})raw"));
    break;
  case Stopwatch::State::STOP:
    ws->textAll(String(R"raw({"state":"Measure finished"})raw"));
    break;
  }
}

// transmit speeds trough the given websocket
void transmitMeasureSpeeds(AsyncWebSocket *ws, float speed_mps,
                           float speed_kmph) {
  char buf[7];
  String data;
  snprintf(buf, 7, "%.2f", speed_mps);
  data = String(R"({"speed_mps":"speed m/s: )") + buf + R"("})";
  ws->textAll(data);
  snprintf(buf, 7, "%.2f", speed_kmph);
  data = String(R"({"speed_kmph":"speed km/h: )") + buf + R"("})";
  ws->textAll(data);
}

// handle message content received by websocket.
void handleWebSocketMessage(AsyncWebSocket *ws_server, void *arg, uint8_t *data,
                            size_t len) {
  AwsFrameInfo *info = (AwsFrameInfo *)arg;
  if (info->final && info->index == 0 && info->len == len &&
      info->opcode == WS_TEXT) {
    data[len] = 0;
    if (strcmp((char *)data, "reset_measure") == 0) {
      gStopwatch.reset();
    }
  }
}

// websocket event handler
void onEvent(AsyncWebSocket *ws_server, AsyncWebSocketClient *client,
             AwsEventType type, void *arg, uint8_t *data, size_t len) {
  switch (type) {
  case WS_EVT_CONNECT:
    Serial.printf("WebSocket client #%u connected from %s\n", client->id(),
                  client->remoteIP().toString().c_str());
    transmitMeasureState(ws_server, gStopwatch.getState());
    transmitMeasureSpeeds(ws_server, gStopwatch.getSpeedMPS(),
                          gStopwatch.getSpeedKmPH());
    break;
  case WS_EVT_DISCONNECT:
    Serial.printf("WebSocket client #%u disconnected\n", client->id());
    break;
  case WS_EVT_DATA:
    handleWebSocketMessage(ws_server, arg, data, len);
    break;
  case WS_EVT_PONG:
  case WS_EVT_ERROR:
    break;
  }
}

// processor for config page
String configProcessor(const String &var) {
  if (var == "DISTANCE_LOW")
    return String(gConfig.getDistanceLow());
  else if (var == "DISTANCE_HIGH")
    return String(gConfig.getDistanceHigh());
  else if (var == "MEASURING_DISTANCE")
    return String(gConfig.getMeasuringDistance());
  else
    return var;
}

// processor for measure page
String measureProcessor(const String &var) { return String(); }

// request handler for the config page
void configHandler(AsyncWebServerRequest *request) {
  if (request->hasParam("distance_low")) {
    auto val = request->getParam("distance_low")->value().toInt();
    gConfig.setDistanceLow(val);
  }
  if (request->hasParam("distance_high")) {
    auto val = request->getParam("distance_high")->value().toInt();
    gConfig.setDistanceHigh(val);
  }
  if (request->hasParam("measuring_distance")) {
    auto val = request->getParam("measuring_distance")->value().toInt();
    gConfig.setMeasuringDistance(val);
  }
  request->send_P(200, "text/html", CONFIG_HTML, configProcessor);
}

void measureHandler(AsyncWebServerRequest *request) {
  request->send_P(200, "text/html", MEASURE_HTML, measureProcessor);
}

WebInterface::WebInterface(uint16_t port) : _server(port), _ws("/ws") {

  // setup websocket
  _ws.onEvent(onEvent);
  _server.addHandler(&_ws);

  // set some request handler depending we have stopwatch or not
  _server.on("/", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->redirect("/measure");
  });
  _server.on("/measure", HTTP_GET, measureHandler);

  // handler for config page
  _server.on("/config", HTTP_GET, configHandler);

  // handler to deserve style sheet
  _server.on("/style.css", HTTP_GET, [](AsyncWebServerRequest *request) {
    request->send_P(200, "text/css", STYLE_CSS);
  });
}

void WebInterface::begin() { _server.begin(); }

void WebInterface::end() { _server.end(); }

void WebInterface::cleanupClients() { _ws.cleanupClients(); }

void WebInterface::updateMeasureState() {
  transmitMeasureState(&_ws, gStopwatch.getState());
}

void WebInterface::updateMeasureSpeed() {
  transmitMeasureSpeeds(&_ws, gStopwatch.getSpeedMPS(),
                        gStopwatch.getSpeedKmPH());
}
