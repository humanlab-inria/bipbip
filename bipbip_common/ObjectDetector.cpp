#include "ObjectDetector.h"

bool ObjectDetector::init() {
  Wire.begin(0, 26, 400000UL); // start and join i2c bus
  _sensor.setTimeout(500);
  _is_init = _sensor.init();
  if (!_is_init) {
    return false;
  }

  // initialize history
  _presence_event = false;
  _distance = _sensor.readRangeSingleMillimeters();
  //read global configuration;
  auto distanceLow = gConfig.getDistanceLow();
  auto distanceHigh = gConfig.getDistanceHigh();
  // hysteresis
  if (_distance <= distanceLow) {
    _is_present = true;
    _is_present_filtered = true;
  } else if (_distance > distanceHigh) {
    _is_present = false;
    _is_present_filtered = false;
  }
  _sensor.startContinuous(); // enable continuous mode
  return _is_init;
}

bool ObjectDetector::is_init() { return _is_init; }

void ObjectDetector::update() {
  bool last_is_present = _is_present;
  bool last_is_present_filtered = _is_present_filtered;
  unsigned long millis_ = millis();
  _distance = _sensor.readRangeContinuousMillimeters();
  //read global configuration;
  auto distanceLow = gConfig.getDistanceLow();
  auto distanceHigh = gConfig.getDistanceHigh();

  // hysteresis
  if (_distance <= distanceLow) {
    _is_present = true;
  } else if (_distance > distanceHigh) {
    _is_present = false;
  }

  // filter: presence is considered immediately but absence need a certain
  // duration
  if (_is_present) {
    _is_present_filtered = true;
  } else if ((millis_ - _last_change) > 500) {
    _is_present_filtered = false;
  }

  // presence event
  _presence_event = (_is_present_filtered && !last_is_present_filtered);

  if (last_is_present != _is_present) {
    _last_change = millis_;
  }
}

uint16_t ObjectDetector::distance() { return _distance; }

bool ObjectDetector::isPresentFiltered() { return _is_present_filtered; }

bool ObjectDetector::presenceEvent() { return _presence_event; }
