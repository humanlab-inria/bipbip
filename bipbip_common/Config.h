#ifndef BIPBIP_CONFIG_H
#define BIPBIP_CONFIG_H

#include "common.h"
#include <Preferences.h>
#include <nvs_flash.h>

// Default ToF detection thresholds
const uint16_t DEFAULT_DISTANCE_LOW = 1000;
const uint16_t DEFAULT_DISTANCE_HIGH = 1200;

// Default distance in meter between the 2 checkpoints
const float DEFAULT_MEASURING_DISTANCE = 5.00;

// This class manage the global configuration, including load and store from
// non-volatile memory.
//
// Note for "distance low" and "distance high". Under the "low" threshold, the
// detector assume something is present, above "high" threshold, the detector
// assume there is nothing.  Between "low" and "high", the detector use the
// previous assumption
class Config {
private:
  const char *_namespace = "BipBipConfig";
  Preferences _prefs;
  // Tof detection thresholds. Attribute prefixed with "last" keep track of
  // last saved value.
  uint16_t _distanceLow;
  uint16_t _lastDistanceLow;
  uint16_t _distanceHigh;
  uint16_t _lastDistanceHigh;
  // distance between the 2 checkpoint
  uint16_t _measuringDistance;
  uint16_t _lastMeasuringDistance;

public:
  // ctor
  Config();

  // Get the threshold "low" of the detector.
  uint16_t getDistanceLow() { return _distanceLow; }

  // Set the threshold "low" of the detector.
  void setDistanceLow(uint16_t value) { _distanceLow = value; }

  // Get the threshold "high" of the detector.
  uint16_t getDistanceHigh() { return _distanceHigh; }

  // Set the threshold "high" of the detector.
  void setDistanceHigh(uint16_t value) { _distanceHigh = value; }

  // Get the distance between the two checkpoint for the speed measurement.
  uint16_t getMeasuringDistance() { return _measuringDistance; }

  // Set the distance between the two checkpoint for the speed measurement.
  void setMeasuringDistance(uint16_t value) { _measuringDistance = value; }

  // Load configuration from non-volatile memory. If a configuration data is
  // not found it load a default value instead. Mean to be and should exactly
  // called once at startup
  void load();
  // Store configuration to non-volatile memory. To prevent memory wearing,
  // values are actually stored only if they changed between the previous call.
  // Mean to be and should only called from the main loop context. In
  // particular, calling it from 2 different "threads" may cause data
  // corruption or deadlock.
  void store();
};

extern Config gConfig;

#endif // BIPBIP_CONFIG_H
