#include <VL53L0X.h>
#include "Config.h"


// This class read values returned by the ToF sensor and "trigger" a presence
// event when is present. You need to call the "update()" function first in
// each loop to actually read the ToF sensor and update internal state of
// ObjectDetector. This class require the external global gConfig object.
class ObjectDetector {
private:
  VL53L0X _sensor;
  bool _is_init;
  uint16_t _distance;
  bool _is_present;
  bool _is_present_filtered;
  bool _presence_event;
  unsigned long _last_change;

public:
  // Initialize and setup ToF sensor. Return true is sensor have been
  // correctly initialized
  bool init();

  // return true if sensor is correctly initialized
  bool is_init();

  // update internal state, must be called once only at beginning of Arduino
  // loop
  void update();

  // get distance measure of the ToF sensor captured during update
  uint16_t distance();

  // get the output of the detector. Return true when the detector assume
  // something is present
  bool isPresentFiltered();

  // return true on new detection, that is something is present now, but there
  // where nothing before
  bool presenceEvent();
};
