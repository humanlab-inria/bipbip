#ifndef BIPBIP_STOPWATCH_H
#define BIPBIP_STOPWATCH_H
#include "Config.h"
#include <Arduino.h>

// human readable representation of stopwatch time
struct StopwatchTime {
  uint8_t min = 0;   // minutes
  uint8_t sec = 0;   // second
  uint8_t centi = 0; // centi second
};

// class implementing basic stopwatch functionality and speed measurement.
class Stopwatch {
public:
  enum State : uint8_t {
    INIT, // not running and no measure
    RUN,  // measuring time
    STOP, // end of measure
  };

private:
  // Current state of the Stopwatch
  enum State _state = State::INIT;

  // Keep track if state changed.
  bool _state_changed_flag;

  // speed in meter per second
  float _speed_mps = 0;
  // speed in kilometer per hour
  float _speed_kmph = 0;

  // The moment when the stopwatch started
  unsigned long _start_ms = 0;

  // Duration of measure, calculated during update
  unsigned long _duration_ms = 0;

public:
  // get state of the stopwatch
  enum State getState() { return _state; }

  // get duration in ms.
  unsigned long getDuration() { return _duration_ms; }

  // get time in a more human readable representation
  StopwatchTime getTime() {
    StopwatchTime time;
    time.centi = (_duration_ms / 10) % 100;
    time.sec = (_duration_ms / 1000) % 60;
    time.min = (_duration_ms / 60000);
    return time;
  }

  // Get speed in meter per second. Only valid when a measure finished, that
  // mean the stopwatch state is STOP, it return 0 otherwise.
  float getSpeedMPS() { return _speed_mps;}

  // Get speed in kilometer per hour. Only valid when a measure finished, that
  // mean the stopwatch state is STOP, it return 0 otherwise.

  float getSpeedKmPH() { return _speed_kmph;}

  // update internal values, must be called at beginning of each loop
  void update() {
    if (_state == RUN) {
      _duration_ms = millis() - _start_ms;
    }
  }

  // reset the stopwatch. This delete previous duration measure.
  void reset() {
    _state = INIT;
    _state_changed_flag = true;
    _duration_ms = 0;
   _speed_mps = 0;
   _speed_kmph = 0;
  }

  // start new measure. This delete previous measure.
  void restart() {
    _state = RUN;
    _state_changed_flag =true;
    _duration_ms = 0;
   _speed_mps = 0;
   _speed_kmph = 0;
    _start_ms = millis();
  }

  // stop measure. Internally calculate speed
  void stop() {
    _state = STOP;
    _state_changed_flag = true;
    auto measuringDistance = gConfig.getMeasuringDistance();
    _speed_mps = measuringDistance * 1000.0 / ((float)this->getDuration());
    _speed_kmph = _speed_mps * 3600.0 / 1000.0;
  }

  // get value of state changed flag and delete it. This flag is true by reset,
  // restart and stop functions.
  bool takeStateChangedFlag() {
    auto res = _state_changed_flag;
    _state_changed_flag = false;
    return res;
  }
};
#endif // BIPBIP_STOPWATCH_H
