/*
* This file is a part of BipBip
*
* Copyright (C) 2023 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

#include "common.h"

int8_t estimateBatCapacity(float voltage) {
  // clang-format off
  if (voltage < 3.20) return -1;
  if (voltage < 3.27) return 0;
  if (voltage < 3.61) return 5;
  if (voltage < 3.69) return 10;
  if (voltage < 3.71) return 15;
  if (voltage < 3.73) return 20;
  if (voltage < 3.75) return 25;
  if (voltage < 3.77) return 30;
  if (voltage < 3.79) return 35;
  if (voltage < 3.80) return 40;
  if (voltage < 3.82) return 45;
  if (voltage < 3.84) return 50;
  if (voltage < 3.85) return 55;
  if (voltage < 3.87) return 60;
  if (voltage < 3.91) return 65;
  if (voltage < 3.95) return 70;
  if (voltage < 3.98) return 75;
  if (voltage < 4.02) return 80;
  if (voltage < 4.08) return 85;
  if (voltage < 4.11) return 90;
  if (voltage < 4.15) return 95;
  if (voltage < 4.20) return 100;
  else return 101;// voltage >= 4.20
  // clang-format on
}

