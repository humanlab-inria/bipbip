#ifndef BIPBIP_WEB_INTERFACE_H
#define BIPBIP_WEB_INTERFACE_H

#include "Config.h"
#include "Stopwatch.h"
#include <ESPAsyncWebSrv.h>

extern Config gConfig;
extern Stopwatch gStopwatch;

// Web interface. Require external gConfig and gStopwatch objects.
//
// Note: the web interface access to Stopwatch through a global variable
// because i's the only i found to get stopwatch data inside a request handler.
class WebInterface {
private:
  // standard web server
  AsyncWebServer _server;
  // web socket
  AsyncWebSocket _ws;

public:
  // Constructor. port is the TCP port where the server is listening
  WebInterface(uint16_t port);

  // Start the WebInterface, that mean, starting the web server
  void begin();

  // Stop the WebInterface, that mean, stopping the web server
  void end();

  // Delete too old client
  void cleanupClients();

  // Update update measure state in the web interface
  void updateMeasureState();

  // Update speed displayed on the web interface
  void updateMeasureSpeed();
};
#endif // BIPBIP_WEB_INTERFACE_H
