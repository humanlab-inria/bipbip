#include "Config.h"

#define RW_MODE false
#define RO_MODE true

Config::Config()
    : _distanceLow(DEFAULT_DISTANCE_LOW),
      _lastDistanceLow(DEFAULT_DISTANCE_LOW),
      _distanceHigh(DEFAULT_DISTANCE_HIGH),
      _lastDistanceHigh(DEFAULT_DISTANCE_HIGH),
      _measuringDistance(DEFAULT_MEASURING_DISTANCE),
      _lastMeasuringDistance(DEFAULT_MEASURING_DISTANCE) {}

void Config::load() {
  _prefs.begin(_namespace, RO_MODE);
  _distanceLow = _prefs.getUShort("distanceLow", DEFAULT_DISTANCE_LOW);
  _distanceHigh = _prefs.getUShort("distanceHigh", DEFAULT_DISTANCE_HIGH);
  _measuringDistance =
      _prefs.getUShort("measuringDistance", DEFAULT_MEASURING_DISTANCE);
  _prefs.end();
  _lastDistanceLow = _distanceLow;
  _lastDistanceHigh = _distanceHigh;
  _lastMeasuringDistance = _measuringDistance;
}

void Config::store() {
  if (_distanceLow != _lastDistanceLow) {
    _prefs.begin(_namespace, RW_MODE);
    _prefs.putUShort("distanceLow", _distanceLow);
    _prefs.end();
    _lastDistanceLow = _distanceLow;
  }

  if (_distanceHigh != _lastDistanceHigh) {
    _prefs.begin(_namespace, RW_MODE);
    _prefs.putUShort("distanceHigh", _distanceHigh);
    _prefs.end();
    _lastDistanceHigh = _distanceHigh;
  }

  if (_measuringDistance != _lastMeasuringDistance) {
    _prefs.begin(_namespace, RW_MODE);
    _prefs.putUShort("measuringDistance", _measuringDistance);
    _prefs.end();
    _lastMeasuringDistance = _measuringDistance;
  }
}
