/*
* This file is a part of BipBip
*
* Copyright (C) 2023 Inria
*
* This software is governed by the CeCILL license under French law and
* abiding by the rules of distribution of free software.  You can  use, 
* modify and/ or redistribute the software under the terms of the CeCILL
* license as circulated by CEA, CNRS and INRIA at the following URL
* "http://www.cecill.info". 
*
* As a counterpart to the access to the source code and  rights to copy,
* modify and redistribute granted by the license, users are provided only
* with a limited warranty  and the software's author,  the holder of the
* economic rights,  and the successive licensors  have only  limited
* liability.
*/

#include "src/common/Config.h"
#include "src/common/ObjectDetector.h"
#include "src/common/WebInterface.h"
#include "src/common/Stopwatch.h"
#include "src/common/common.h"
#include <M5StickCPlus.h>
#include <WiFi.h>
#include <WiFiMulti.h>

Config gConfig;
ObjectDetector detector;

WiFiClient client;

// Required because of workaround for the webinterface
Stopwatch gStopwatch;

WebInterface web_interface(80);

void setup() {
  // Display setup
  M5.begin();
  M5.Lcd.setRotation(3); // Rotate the screen.
  M5.lcd.setTextSize(2);
  M5.lcd.println("BipBip Slave");

  // detector init
  if (!detector.init()) {
    M5.lcd.println("Error, sensor not found");
    while (true)
      ;
  }

  // setup the pin that control the led
  pinMode(10, OUTPUT);

  // WiFi setup
  WiFi.begin(SSID, PASSWORD);

  web_interface.begin();
}

void loop() {
  // buffer for string manipualtion
  const auto print_buf_sz = 128;
  char print_buf[print_buf_sz];

  auto font_height = M5.lcd.fontHeight();
  auto bat_voltage = M5.Axp.GetBatVoltage();
  auto bat_current = M5.Axp.GetBatCurrent();
  auto bat_capacity = estimateBatCapacity(bat_voltage);

  long int wifi_strength_dbm = WiFi.RSSI();
  char wifi_strength_txt[16] = {'\0'};
  if (wifi_strength_dbm != 0) {
    snprintf(wifi_strength_txt, 16, "% 3ld dbm", wifi_strength_dbm);
  }

  // y coordinate we currently writing at
  auto cur_y = font_height;

  detector.update();
  // light up the led when something is present;
  digitalWrite(10, !detector.isPresentFiltered());

  // wait for WiFi connection
  const char *wifi_status_txt = "not connected";
  char local_ip_txt[16] = {'\0'};
  char gateway_ip_txt[16] = {'\0'};
  auto localIP = WiFi.localIP();
  auto gateway_ip = WiFi.gatewayIP();

  if ((WiFi.status() == WL_CONNECTED)) {
    wifi_status_txt = "connected";
    if (detector.presenceEvent()) {
      Serial.println("object detected");
      // TODO don't hardcode IP and port
      if (client.connect(gateway_ip, MASTER_PORT)) {
        client.setNoDelay(true); // immediatly send buffer content
        client.write(1);         // send message
        // check if connection closed by remote
        if (!client.connected()) {
          Serial.println("Connection closed by remote");
        }
        client.stop();
      } else {
        Serial.println("Can't connect to server");
      }
    }
  }

  M5.lcd.setTextPadding(240); // to erase old text when writing new

  const char *charging_msg = "";
  if (bat_current > 0.1) {
    charging_msg = "charging";
  }

  snprintf(print_buf, print_buf_sz, "IP: %u.%u.%u.%u", localIP[0], localIP[1],
           localIP[2], localIP[3]);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  snprintf(print_buf, print_buf_sz, "Bat: %3d %% %s", bat_capacity,
           charging_msg);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  snprintf(print_buf, print_buf_sz, "WiFi: %s", wifi_status_txt);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  snprintf(print_buf, print_buf_sz, "Signal: %s", wifi_strength_txt);
  M5.lcd.drawString(print_buf, 0, cur_y);
  cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "SSID: %s", SSID);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "Password: %s", PASSWORD);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "Gateway: %u.%u.%u.%u", gateway_ip[0],
  //          gateway_ip[1], gateway_ip[2], gateway_ip[3]);
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;

  // snprintf(print_buf, print_buf_sz, "ToF: %4umm ", detector.distance());
  // M5.lcd.drawString(print_buf, 0, cur_y);
  // cur_y += font_height;
}
