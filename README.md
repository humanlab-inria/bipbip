# BipBip

Dispositif de mesures de vitesse de marche à base de 2 balises de détection.

## Matériel

Chaque balise est constituée par :

* 1 [M5StickC-Plus](https://shop.m5stack.com/products/m5stickc-plus-esp32-pico-mini-iot-development-kit),
kit de développement basé sur un ESP32
* 1 [ToF HAT VL53l0x](https://shop.m5stack.com/products/m5stickc-tof-hatvl53l0x),
module de capteur de distance qui est ici utilisé comme détecteur de présence

Les 2 balises communiquent par Wifi: une balise fait office de
passerelle et l'autre se connecte dessus. Le principe est que la
balise d'entrée détecte le passage du marcheur et communique
l'évènement à la balise de sortie. La balise de sortie mesure le délai
temporel entre la réception de cet évènement et la détection de
passage du marcheur en sortie. Connaissant la distance entre la balise
d'entrée et de sortie, la balise de sortie est alors en mesure de
calculer la vitesse de marche.

## Code Arduino

Le code Arduino est réparti en 3 dossier :

* `bipbip_master`, sketch de la balise "maitre". Il s'agit de la balise de
  sortie qui fait office de passerelle et qui mesure et calcul la vitesse de
marche.
* `bipbip_slave`, sketch de la balise "esclave" qui est aussi la balise
  d'entrée.
* `bipbip_common`, code partagé entre les 2 sketch. Chaque sketch contient un
  lien symbolique `src/common` pour accéder à ce code partagé.

### bibliothèque utilisées

* **VL53L0X by polulu (1.3.0):** pilotage du capteur Time of Light

## Manuel d'utilisation

Le dispositif est constitué de 2 balises : 
* une balise "maitre" qui effectue la mesure du temps et le calcul de la
  vitesse et qui fourni un réseau WiFi pour la balise "esclave"
* une balise "esclave" qui se connecte à la balise "maitre" et qui lui envoie
  un message chaque fois qu'un objet ou une personne est détecté

### Présentation d'une balise

![Schéma Balise](image/Balise.svg)

* **Bouton Power**: Un appuis court permet d'allumer la balise. Appuyer 6
  secondes pour éteindre la balise.
* **Bouton A**: Sur la balise maitre, permet de réinitialiser le chronomètre et
  la mesure de vitesse.  Inutilisé sur la balise esclave.
* **Bouton B**: Inutilisé.
* **LED (Cachée)**: La LED se trouve à l'intérieur du boitier, Elle s'allume
  lorsque qu'un objet ou une personne est détecté par la balise.
* **Capteur de proximité**: Se trouve sur le coté du boitier.
* **Écran d'affichage**: Affiche des informations propre à chaque balise.

#### Affichage de la balise maitre

* **BipBip Master:** Indique qu'il s'agit de la balise "maitre"
* **Bat:** Indique le niveau de la batterie. Le message "charging" apparais
  lorsque la balise est branché en USB et en charge.
* **time:** Chronomètre, indique la durée de la mesure
* **speed m/s:** Indique en mètre par seconde la vitesse calculée à la fin
  d'une mesure.
* **speed km/h:** Indique en kilomètre par heure la vitesse calculée à la fin
  d'une mesure.

#### Affichage de la balise esclave

* **BipBip Slave:** Indique qu'il s'agit de la balise "esclave"
* **Bat:** Indique le niveau de la batterie. Le message "charging" apparais
  lorsque la balise est branché en USB et en charge.
* **WiFi:** Indique si la balise est connectée à la balise maitre.
* **Signal:** Indique la puissance du signal WiFi en dbm lorsque la balise est
  connecté.

### Installation du dispositif

![Schéma Installation](image/Installation.svg)

Les 2 balises doivent être disposées à 5 mètres l'une de l'autre. La balise
esclave (BipBip slave) se place au point d'entrée et la balise maitre (BipBip
master) se place au point de sortie.

### Déroulé d'une mesure

1. Réinitialisez la balise maitre en appuyant sur le bouton A. Le chronomètre et
les vitesses devrais être remis à zéro.
2. La mesure commence lorsqu'une personne passe devant la balise esclave. La
balise maitre devrais émettre un bip court et son chronomètre devrais commencer
à compter.
3. La mesure se termine lorsque la personne passe devant la balise maitre. La
balise maitre devrais émettre un bip long, son chronomètre devrais s'arrêter de
compter et la vitesse calculé devrais s'afficher en m/s et en km/h.

## Licence

Ce logiciel est sous licence [Ceccil](./COPYING)